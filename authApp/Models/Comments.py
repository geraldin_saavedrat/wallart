from django.db import models
from .Post import Post
from .User import User

class Comments(models.Model):
    idComments = models.AutoField(primary_key=True)
    commentContent = models.CharField(max_length=100)
    idUserFK = models.ForeignKey(User, related_name='Comments_idUserFK', on_delete=models.CASCADE)
    idPostFK = models.ForeignKey(Post, related_name='Comments_idPostFK', on_delete=models.CASCADE)
    dateComment = models.DateField()